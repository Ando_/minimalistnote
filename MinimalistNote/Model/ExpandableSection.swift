//
//  ExpandableSection.swift
//  MinimalistNote
//
//  Created by Shaun Anderson on 13/8/18.
//  Copyright © 2018 Shaun Anderson. All rights reserved.
//

import Foundation

struct ExpandableSection {
    var isExpanded: Bool
    var notes: [Note]
}
