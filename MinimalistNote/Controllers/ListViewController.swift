//
//  ListController.swift
//  MinimalistNote
//
//  Created by Shaun Anderson on 11/8/18.
//  Copyright © 2018 Shaun Anderson. All rights reserved.
//

import UIKit
import CoreData

class ListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var appDelegate: AppDelegate!
    var context : NSManagedObjectContext!
    
    var notebooks : [Notebook] = []
    var notes = [ExpandableSection]()
    
    var placeHolder : TableSectionHeader?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self;
        tableView.dataSource = self;
        // Get Nibs
        let nib = UINib(nibName: "TableSectionHeader", bundle: nil)
        let placeHolderNib = UINib(nibName: "PlaceholderTableViewCell", bundle: nil)
        tableView.register(placeHolderNib, forHeaderFooterViewReuseIdentifier: "PlaceholderTableViewCell")
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: "TableSectionHeader")
        
        // Access CoreData context
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
                
        // Get all notebooks created.
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Notebook")
        let sort = NSSortDescriptor(key: #keyPath(Notebook.sectionIndex), ascending: false)
        request.sortDescriptors = [sort]
        
        do {
            let result = try context.fetch(request)
            notebooks = result as! [Notebook]
        } catch {
            print("Failed to retrieve notebooks.")
        }
        
        // Loop through and retrieve all notes for specfic notebooks.
        for notebook in notebooks {
            var i : Int = 0
            let newSection:ExpandableSection = ExpandableSection(isExpanded: true, notes: (Array(notebook.relationship!) as! [Note]))
            notes.append(newSection)
            i += 1
        }
        
        print(notes)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "listToNote" {
            let noteDetailViewController = segue.destination as! NoteViewController
            noteDetailViewController.note = notes[(tableView.indexPathForSelectedRow?.section)!].notes[(tableView.indexPathForSelectedRow?.row)!]
            noteDetailViewController.appDelegate = appDelegate
            noteDetailViewController.context = context
        }
    }
    
    /**
     handleExpandClose
     Handle the functionaility of the collapsable sections.
     **/
    func handleExpandClose (sectionIndex: Int) {
        
        if(!notes[sectionIndex].isExpanded)
        {
            print("handling expand for section: \(sectionIndex)")

            notes[sectionIndex].notes = (Array(notebooks[sectionIndex].relationship!) as! [Note])
            var paths = [IndexPath]()
            for index in 0...notes[sectionIndex].notes.count - 1 {
                paths.append(IndexPath(item: index, section: sectionIndex))
            }
            tableView.insertRows(at: paths, with: UITableViewRowAnimation.top)

            notes[sectionIndex].isExpanded = true
        }
        else
        {
            print("handling close for section: \(sectionIndex)")
            if(notes[sectionIndex].notes.count > 0) {
                for index in 0...notes[sectionIndex].notes.count - 1 {
                    notes[sectionIndex].notes.removeFirst()
                    let newIndex: IndexPath = IndexPath(item: 0, section: sectionIndex)
                    tableView.deleteRows(at: [newIndex], with: UITableViewRowAnimation.top)
                }

                notes[sectionIndex].isExpanded = false
            }
        }
        
    }
    
    func addNote (sectionIndex : Int) {
        
        print("Adding note to section: \(sectionIndex)")
        
        // Create a new note
        let newNote = Note(context: context)
        newNote.title = ""
        notebooks[sectionIndex].addToRelationship(newNote)
        newNote.noteRelationship = notebooks[sectionIndex]
        newNote.text = "Test text"
        newNote.noteID = NSUUID() as UUID
        
        // Add to array
        notes[sectionIndex].notes.append(newNote)
        
        // Update Tableview
        let newIndex: IndexPath = IndexPath(item: notes[sectionIndex].notes.count-1, section: sectionIndex)
        tableView.insertRows(at: [newIndex], with: UITableViewRowAnimation.automatic)
        
        
        ContextSave()

    }
    @IBAction func addNotebookButtonPressed(_ sender: Any) {
        addNotebook(sectionIndex: 0)
    }
    
    func ContextSave ()
    {
        do {
            try context.save()
        } catch {
            print("Failed saving: \(error)")
        }
    }
    
    func addNotebook (sectionIndex: Int) {
        
        // Add to array
        let newNotebook = Notebook(context: context)
        newNotebook.title = ""
        newNotebook.notebookID = NSUUID() as UUID
        //newNotebook.color = UIColor.randomised().toHex
        notebooks.insert(newNotebook, at: 0)
        notes.insert(ExpandableSection(isExpanded: true, notes: []), at: 0)
        
        tableView.beginUpdates()
        tableView.insertSections([0], with: .top)
        tableView.endUpdates()
        
        let editIndex = IndexPath(row: 0, section: 0)
        let headerView = tableView.headerView(forSection: editIndex.section) as! TableSectionHeader
        headerView.delegate = self
        headerView.editing = true
        headerView.notebookTitle.becomeFirstResponder()
        
        ContextSave()
        
    }
}

// MARK: - UITableViewDelegate

extension ListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableSectionHeader") as! TableSectionHeader
        cell.delegate = self
        cell.notebookTitle.delegate = self
        cell.notebookTitle.text = notebooks[section].title
        cell.expandButton.tag = section
        cell.expandButton.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
        cell.addButton.tag = section
        cell.addButton.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}

// MARK: - UITableViewDataSource

extension ListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "noteCell", for: indexPath)
            as! NoteTableViewCell
        let note = notes[indexPath.section].notes[indexPath.row]
        cell.titleLabel.text = note.title
        cell.descLabel.text = note.text
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return notebooks.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes[section].notes.count
    }
    
}

// MARK: - HeaderDelegate

extension ListViewController : HeaderDelegate {
    
    func titleDidEndEditing(_ textField: UITextField) {
        print(ContextSave())
        // If empty delete notebook
        if(textField.text == "")
        {
            
        }
        else
        {
            ContextSave()
        }
    }
    
    func expandPressed(_ button: UIButton) {
        print("Expand button pressed for section: \(button.tag)")
        notes[button.tag].isExpanded ? button.setImage(#imageLiteral(resourceName: "Expand"), for: .normal) : button.setImage(#imageLiteral(resourceName: "Close"), for: .normal)
        handleExpandClose(sectionIndex: button.tag)
    }
    
    func addPressed(_ button: UIButton) {
        addNote(sectionIndex: button.tag)
    }
    
}

// MARK: - UITextFieldDelegate

extension ListViewController : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("End editing")
        let editIndex = IndexPath(row: 0, section: 0)
        let headerView = tableView.headerView(forSection: editIndex.section) as! TableSectionHeader
        headerView.editing = false
        notebooks[0].title = textField.text
        ContextSave()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}
