//
//  ViewController.swift
//  MinimalistNote
//
//  Created by Shaun Anderson on 11/8/18.
//  Copyright © 2018 Shaun Anderson. All rights reserved.
//

import UIKit
import CoreData

class NoteViewController: UIViewController {
    
    // MARK: - Properties

    // Outlets
    @IBOutlet weak var titleTextField: BottomBorderTextField!
    @IBOutlet weak var mainTextView: UITextView!
    @IBOutlet weak var wordCountLabel: UILabel!
    @IBOutlet weak var notebookNameLabel: UILabel!
    
    // Constraints
    @IBOutlet weak var footerbottomConstraint: NSLayoutConstraint!
    
    // Variables
    var appDelegate: AppDelegate!
    var context : NSManagedObjectContext!
    var note: Note?
    var notebook: Notebook?
    
    // MARK: - Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        mainTextView.delegate = self
        notebook = note?.noteRelationship as Notebook!
        
        notebookNameLabel.text = notebook?.title
        //self.view.backgroundColor = UIColor(hex: (notebook?.color)!)
        titleTextField.text = note?.title
        mainTextView.text = note?.text
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
        
        wordCountLabel.text = "# \(String(mainTextView.wordCount()))"

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func ContextSave ()
    {
        do {
            try context.save()
        } catch {
            print("Failed saving: \(error)")
        }
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.footerbottomConstraint?.constant = 0.0
            } else {
                self.footerbottomConstraint?.constant = -(endFrame?.size.height)! ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Actions
    
    @IBAction func edgeSwiped(_ sender: UIGestureRecognizer) {
        ContextSave()
        if(sender.state == UIGestureRecognizerState.ended)
        {
            note?.title = titleTextField.text
            self.performSegue(withIdentifier: "noteToList", sender:sender)
        }
    }
    
    
}

// MARK: - UITextFieldDelegate

extension NoteViewController: UITextFieldDelegate {

    func textFieldDidEndEditing(_ textField: UITextField) {
        print("hiohoihiohoih")
        note?.title = textField.text
    }
    
}

// MARK: - UITextViewDelegate

extension NoteViewController: UITextViewDelegate {
    
    func textViewDidEndEditing(_ textView: UITextView) {
        wordCountLabel.text = "# \(String(mainTextView.wordCount()))"
    }
    
    func textViewDidChange(_ textView: UITextView) {
        wordCountLabel.text = "# \(String(mainTextView.wordCount()))"
        note?.text = textView.text
    }
    
}


