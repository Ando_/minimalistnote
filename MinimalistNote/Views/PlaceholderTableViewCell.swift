//
//  PlaceholderTableViewCell.swift
//  MinimalistNote
//
//  Created by Shaun Anderson on 13/8/18.
//  Copyright © 2018 Shaun Anderson. All rights reserved.
//

import UIKit
protocol PlaceholderDelegate {
    func doneEditing (_ textField: UITextField)
}
class PlaceholderTableViewCell: UITableViewCell {

    var delegate: PlaceholderDelegate?
    @IBOutlet weak var nameTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension PlaceholderTableViewCell: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        delegate?.doneEditing(textField)
    }
}
