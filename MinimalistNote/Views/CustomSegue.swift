//
//  CustomSegue.swift
//  MinimalistNote
//
//  Created by Shaun Anderson on 12/8/18.
//  Copyright © 2018 Shaun Anderson. All rights reserved.
//

import UIKit

class CustomSegue: UIStoryboardSegue {
    override func perform()
    {
        print("Segue: ListToNote")

        // Assign the source and destination views to local variables.
        let firstVCView = self.source.view as UIView!
        let secondVCView = self.destination.view as UIView!
        
        // Get the screen width and height.
        let screenWidth = UIScreen.main.bounds.size.width
        let screenHeight = UIScreen.main.bounds.size.height
        
        // Specify the initial position of the destination view.
        secondVCView?.frame = CGRect(x: screenWidth, y: 0, width: screenWidth, height: screenHeight)
        
        // Access the app's key window and insert the destination view above the current (source) one.
        let window = UIApplication.shared.keyWindow
        window?.insertSubview(secondVCView!, aboveSubview: firstVCView!)
        
        let newPos = firstVCView!.frame.offsetBy(dx: -screenWidth, dy: 0.0)
        let new2Pos = secondVCView!.frame.offsetBy(dx: -screenWidth, dy: 0.0)

        // Animate the transition.
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            firstVCView!.frame = newPos
            secondVCView!.frame = new2Pos
            
        }) { (Finished) -> Void in
            self.source.present(self.destination as UIViewController,
                                                            animated: false,
                                                            completion: nil)
        }
    }
}

class NoteToListSegue: UIStoryboardSegue {
    override func perform()
    {
        print("Segue: NoteToList")
        // Assign the source and destination views to local variables.
        let firstVCView = self.source.view as UIView!
        let secondVCView = self.destination.view as UIView!
        
        // Get the screen width and height.
        let screenWidth = UIScreen.main.bounds.size.width
        let screenHeight = UIScreen.main.bounds.size.height
        
        // Specify the initial position of the destination view.
        secondVCView?.frame = CGRect(x: -screenWidth, y: 0, width: screenWidth, height: screenHeight)
        
        // Access the app's key window and insert the destination view above the current (source) one.
        let window = UIApplication.shared.keyWindow
        window?.insertSubview(secondVCView!, aboveSubview: firstVCView!)
        
        let newPos = firstVCView!.frame.offsetBy(dx: screenWidth, dy: 0.0)
        let new2Pos = secondVCView!.frame.offsetBy(dx: screenWidth, dy: 0.0)
        
        // Animate the transition.
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            firstVCView!.frame = newPos
            secondVCView!.frame = new2Pos
            
        }) { (Finished) -> Void in
            self.source.present(self.destination as UIViewController,
                                animated: false,
                                completion: nil)
        }
    }
}
