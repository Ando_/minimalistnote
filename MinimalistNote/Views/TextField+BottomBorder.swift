//
//  BottomBorderTextField.swift
//  MinimalistNote
//
//  Created by Shaun Anderson on 11/8/18.
//  Copyright © 2018 Shaun Anderson. All rights reserved.
//

import Foundation
import UIKit

class BottomBorderTextField : UITextField {
    
    // MARK: - Properties
    @IBInspectable var borderHeight: CGFloat = 0.0
    
    
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 5, right: 10)
    
    override var tintColor: UIColor! {
        
        didSet {
            setNeedsDisplay()
        }
    }
    
    
    // MARK: - Functions
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        let placeholderRect: CGRect = CGRect(x: bounds.origin.x, y: bounds.size.height - self.font!.pointSize - 5, width: bounds.size.width, height: self.font!.pointSize + 1);
        return UIEdgeInsetsInsetRect(placeholderRect, padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func draw(_ rect: CGRect) {
        
        let startingPoint   = CGPoint(x: rect.minX, y: rect.maxY)
        let endingPoint     = CGPoint(x: rect.maxX, y: rect.maxY)
        
        let path = UIBezierPath()
        
        path.move(to: startingPoint)
        path.addLine(to: endingPoint)
        path.lineWidth = borderHeight
        
        tintColor.setStroke()
        
        path.stroke()
    }
}
