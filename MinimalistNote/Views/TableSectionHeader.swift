//
//  CustomHeader.swift
//  MinimalistNote
//
//  Created by Shaun Anderson on 11/8/18.
//  Copyright © 2018 Shaun Anderson. All rights reserved.
//

import UIKit

// MARK: - Header Delegate Declaration

protocol HeaderDelegate {
    func expandPressed (_ button: UIButton)
    func addPressed (_ button: UIButton)
    func titleDidEndEditing (_ textField: UITextField)
}

// MARK: - TableSectionHeader Declaration

class TableSectionHeader: UITableViewHeaderFooterView {
    
    // MARK: - Properties
    
    @IBOutlet weak var notebookTitle: UITextField!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var expandButton: UIButton!
    @IBOutlet weak var background: UIView!
    

    var customColor: UIColor?
    var delegate: HeaderDelegate?
    
    var editing : Bool? {
        willSet (newVar) {
            if(newVar!) {
                print("We are editing")
                notebookTitle.isUserInteractionEnabled = true
                expandButton.isHidden = true
                addButton.isHidden = true
            }
            else {
                notebookTitle.isUserInteractionEnabled = false
                expandButton.isHidden = false
                addButton.isHidden = false
            }
        }
    }
    
    // MARK: - Initilisation
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
    }
    
    // MARK: - Button Press Actions
    
    @IBAction func AddButtonPressed(_ sender: UIButton) {
        delegate?.addPressed(sender)
    }
    
    @IBAction func ExpandButtonPressed(_ sender: UIButton) {
        delegate?.expandPressed(sender)
    }
}
